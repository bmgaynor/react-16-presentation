import React from 'react'

const Bomb = () => <span style={{ fontSize: '4rem' }} aria-label='Bomb' role='img'>💣</span>
const Fire = () => <span style={{ fontSize: '4rem' }} aria-label='Fire' role='img'>🔥</span>
const House = () => <span style={{ fontSize: '4rem' }} aria-label='House' role='img'>🏠</span>

class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch (error, info) {
    console.error(error) // can also send to external log
    this.setState({ hasError: true })
  }

  render () {
    if (this.state.hasError) {
      return <h3><Fire /><Fire /></h3>
    }
    return (
      <div style={{ border: '2px dotted grey' }}>
        {this.props.children}
      </div>
    )
  }
}

class Game extends React.Component {
  constructor (props) {
    super(props)
    this.state = { blownUp: false }
    this.blowUp = this.blowUp.bind(this)
  }
  blowUp () {
    this.setState({blownUp: true})
  }
  render () {
    if (this.state.blownUp) {
      throw new Error('Blown Up')
    }
    return (
      <div onClick={this.blowUp} style={{ padding: '1rem' }}>
        <Bomb />
      </div>
    )
  }
}

const WithErrorBoundry = () => {
  return (
    <div>
      <ErrorBoundary>
        <Game />
      </ErrorBoundary>
      <House />
    </div>
  )
}
const WithOutErrorBoundry = () => {
  return (
    <div>
      <ErrorBoundary villageDead>
        <Game />
        <House />
      </ErrorBoundary>
    </div>)
}

const ErrorBoundryDemo = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
      <WithOutErrorBoundry />
      <WithErrorBoundry />
    </div>
  )
}

export const codeString = `
// ./src/slides/ErrorBoundryDemo.js
class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch (error, info) {
    console.error(error) // can also send to external log
    this.setState({ hasError: true })
  }

  render () {
    if (this.state.hasError) {
      return <h3><Fire /><Fire /></h3>
    }
    return (
      <div style={{ border: '1px solid grey' }}>
        {this.props.children}
      </div>
    )
  }
}`

export default ErrorBoundryDemo
