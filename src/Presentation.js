// Import React
import React from 'react'
import PortalDemo, { codeString as PortalCodeString } from './slides/PortalDemo'
import ErrorBoundryDemo, { codeString as ErrorCodeString } from './slides/ErrorBoundryDemo'

// Import Spectacle Core tags
import { Deck, Heading, ListItem, List,
  Slide, Text, CodePane, Appear, Notes, Image, Fill, Layout, Link
} from 'spectacle'

// Import theme
import createTheme from 'spectacle/lib/themes/default'

// Require CSS
require('normalize.css')

const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#03A9FC',
    quartenary: '#CECECE'
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica'
  }
)

// const Hand = () => <span aria-label='Hand' role='img'>👋</span>
const Boom = () => <span aria-label='Boom' role='img'>💥</span>
const Shroom = () => <span aria-label='Shroom' role='img'>🍄</span>
// const Rainbow = () => <span aira-label='Rainbow' role='img'>🌈</span>
const Fire = () => <span aria-label='Fire' role='img'>🔥</span>
const Baby = () => <span aria-label='Baby' role='img'>👶</span>
const Laptop = () => <span aria-label='Laptop' role='img'>💻</span>
const Bomb = () => <span aria-label='Bomb' role='img'>💣</span>

const introStyles = {
  flex: '0 1 50%', overflowWrap: 'break-word', margin: '10px'
}

export default class Presentation extends React.Component {
  render () {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}>
        <Slide transition={['fade']} bgColor='primary' textColor='tertiary'>
          <Heading caps>Slides</Heading>
          <Text textColor='secondary'>react-16-presentation.surge.sh</Text>
          <Link href='https://gitlab.com/bmgaynor/react-16-presentation'>Repo</Link>
        </Slide>
        <Slide transition={['fade']} bgColor='#000000'>
          <Image src='./assets/React-intro-logo.jpg' />
          <Heading textColor='tertiary'>React 16</Heading>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textSize='50px' textColor='primary' caps>
          Zach Willard
          </Heading>
          <List style={{listStyleType: 'none', display: 'flex', justifyContent: 'center', paddingLeft: 0}}>
            <ListItem style={introStyles}>
              <List style={{listStyleType: 'none', paddingLeft: 0}}>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'><Fire /> Engineer at NM working on some sweet React apps</ListItem>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'><Laptop /> Returned to development 10 months ago from Project Management - Learning like crazy</ListItem>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'><Baby /> When not coding he spends his evenings chasing his 15 month daughter around</ListItem>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'>Check me out on Github - @ZachAttack83</ListItem>
              </List>
            </ListItem>
            <ListItem style={introStyles}>
              <Image src={'./assets/Zach.jpg'}/>
            </ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textSize='50px' textColor='primary' caps>
            Brad Gaynor
          </Heading>
          <List style={{listStyleType: 'none', display: 'flex', justifyContent: 'center', paddingLeft: 0}}>
            <ListItem style={introStyles}>
              <List style={{listStyleType: 'none', paddingLeft: 0}}>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'><Fire />Software Engineer for 3 Years at NM</ListItem>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'><Laptop />Been working in React for 2 years </ListItem>
                <ListItem style={{marginBottom: '10px'}} textColor='secondary' textSize='30'><Bomb />@bmgaynor on Github & Gitlab</ListItem>
              </List>
            </ListItem>
            <ListItem style={introStyles}>
              <Image src='https://avatars3.githubusercontent.com/u/3483972' />
            </ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='primary' textColor='tertiary'>
          <Heading size={6} caps>
            React 16 new features
          </Heading>
          <List style={{listStyle: 'none'}}>
            <ListItem>Portals</ListItem>
            <ListItem>Error Boundaries</ListItem>
            <ListItem>Fragments</ListItem>
            <ListItem>Server side rendering (hydrate)</ListItem>
            <ListItem>Streaming</ListItem>
            <ListItem>Fiber</ListItem>
            <ListItem>JS Environment Requirements</ListItem>
          </List>
          <Notes>
            <h4>Slide notes</h4>
            <ol>
              <li>First note</li>
              <li>Second note</li>
            </ol>
          </Notes>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>Portals</Heading>
          <Text textColor='secondary'>Portals are great for extending your React application into a DOM node outside of the parent component of your React app.</Text>
          <Appear fid='1'><Text textColor='secondary'>You can even extend your react application outside of the browser window!</Text></Appear>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <PortalDemo />
          <Link padding='1rem 0' href='https://hackernoon.com/using-a-react-16-portal-to-do-something-cool-2a2d627b0202'>article</Link>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>Code</Heading>
          <CodePane
            lang='jsx'
            theme='light'
            source={PortalCodeString}
            margin='10px auto'
            overflow='overflow' />
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>Before 16</Heading>
          <Text textColor='secondary'>Runtime errors could cause unpredictable behavior in React and leave your UI in a broken state.</Text>
          <Appear fid='1'>
            <List style={{listStyle: 'none'}} bgColor='red' padding='10px'>
              <ListItem style={{lineHeight: '2rem'}} textSize='1.5rem'>Uncaught TypeError: Cannot read property '_currentElement' of null</ListItem>
              <ListItem style={{listStyle: '2rem'}} textSize='1.5rem'>Error: performUpdateIfNecessary: Unexcpeted batch number (current 36, pending 31)(...)</ListItem>
              <ListItem style={{listStyle: '2rem'}} textSize='1.5rem'>Cannot read property 'getHostNode' of null </ListItem>
            </List>
          </Appear>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>Error Boundaries</Heading>
          <Text textColor='secondary'>Catch JavaScript errors anywhere in their child component tree, log those errors, and display a fallback UI</Text>
        </Slide>
        <Slide transition={['fade']} bgColor='secondary' textColor='tertiary'>
          <Heading textColor='tertiary'>Demo</Heading>
          <ErrorBoundryDemo />
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>Code</Heading>
          <CodePane
            lang='jsx'
            theme='light'
            source={ErrorCodeString}
            margin='10px auto'
            overflow='overflow' />
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>What they dont do</Heading>
          <List textColor='secondary'>
            <ListItem>They dont catch errors in Event Handlers</ListItem>
            <ListItem>In Asynchronous code</ListItem>
            <ListItem>During server side rendering</ListItem>
            <ListItem>Within the error boundary itself</ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'>Fragments<Boom /></Heading>
          <List textColor='secondary'>
            <ListItem>React 16.2</ListItem>
            <ListItem>Eliminate divs around children nodes</ListItem>
            <ListItem>Clean up your DOM and avoid:</ListItem>
            <Image src='./assets/div-hell.gif' />
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary' maxHeight='300' >
          <Heading textColor='primary'>CODE</Heading>
          <Layout>
              <Fill>
                <Text>Before</Text>
              </Fill>
              <Fill>
                <Text>Fragments</Text>
              </Fill>
              <Fill>
                <Text>Empty JSX Fragments*</Text>
              </Fill>
          </Layout>
          <Layout>
            <Fill>
            <CodePane
            lang='jsx'
            theme='light'
            source={`render() {
  return (
    <div>
      <ChildA />
      <ChildB />
      <ChildC />
    </div>
  );
}`}
            textSize='20'
            />
            </Fill>
            <Fill>
            <CodePane
              lang='jsx'
              theme='light'
              source={`render() {
  return (
    <React.Fragment>
      <ChildA />
      <ChildB />
      <ChildC />
    </React.Fragment>
  );
}`}
              textSize='20'
            />
            </Fill>
            <Fill>
            <CodePane
              lang='jsx'
              theme='light'
              source={`render() {
  return (
    <>
      <ChildA />
      <ChildB />
      <ChildC />
    </>
  );
}`}
              textSize='20'
            />
            </Fill>
          </Layout>
          <Text textSize='20'>* Requires Babel v7.0.0-beta.31^</Text>
          <Notes>
            <h4>slide notes</h4>
            <ol>
              <li>Can also `const Fragment = React.Fragment`</li>
              <li>You need a beta version of Babel for empty JSX</li>
            </ol>
          </Notes>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textColor='primary'><Fire />Server-side rendering<Fire /></Heading>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary' textColor='primary'>
          <Heading textSize='50'>SSR is 3x faster in React 16 vs 15</Heading>
          <Image src='./assets/React16SSR-perf.png' />
          <Notes>
            <h4>React 16 is roughly 3x faster at rendering than React 15!</h4>
          </Notes>
        </Slide>
        <Slide bgColor='tertiary' textColor='primary'>
          <List>
            <ListItem>React SSR engine was completely re-written</ListItem>
            <ListItem>`process.env` (very expensive) is now complied to read only once</ListItem>
            <ListItem>A lot more efficient HTML is generated</ListItem>
          </List>
        </Slide>
        <Slide bgColor='tertiary' textColor='primary'>
              <Text>React 15</Text>
            <CodePane
              theme='light'
              textSize='20'
            lang='html'
            source={`<div data-reactroot="" data-reactid="1" 
    data-react-checksum="122239856">
  <!-- react-text: 2 -->This is some <!-- /react-text -->
  <span data-reactid="3">server-generated</span>
  <!-- react-text: 4--> <!-- /react-text -->
  <span data-reactid="5">HTML.</span>
</div>`} />

            <Text>React 16</Text>

              <CodePane
              theme='light'
              textSize='20'
            lang='html'
            source={`<div data-reactroot="">
  This is some <span>server-generated</span> <span>HTML.</span>
</div>`} />
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textSize='70' textColor='primary'>hydrate() replaces render()</Heading>
          <CodePane
            theme='light'
            textSize='20'
            source={`import { render } from "react-dom"
import MyPage from "./MyPage"
render(<MyPage/>, document.getElementById("content"))`}
            lang='jsx'
            />
          <CodePane
          textSize='20'
          theme='light'
            lang='jsx'
            source={`import { hydrate } from "react-dom"
import MyPage from "./MyPage"
hydrate(<MyPage/>, document.getElementById("content"))`}
          />
          <Text textColor='primary'>You can still use render() for client side code though!</Text>
          <Notes>
            <ol>
              <li>render() still works</li>
              <li>but it will be deprecated for SSR in React 17</li>
            </ol>
          </Notes>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary' textSize='60'><Shroom/> Level up with Streaming <Shroom/></Heading>
          <List>
            <ListItem>Render to a Node Stream instead of classic SSR</ListItem>
            <ListItem>Send HTML to client as it becomes available while React is still rendering</ListItem>
            <ListItem>Gets content to the browser sooner</ListItem>
            <ListItem>Backpressure - SSR rendering slows down if a buffer builds up, easing on processing and I/O</ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary' textSize='60'><Shroom/> Level up with Streaming <Shroom/></Heading>
          <List>
            <ListItem>Use react-dom/server: renderToNodeStream or renderToStaticNodeStream, which correspond to renderToString and renderToStaticMarkup</ListItem>
            <ListItem>Instead of returning a String, these methods will return a Readable Stream</ListItem>
            <ListItem>pipe() this into your response object, and then when the stream ends, send your final html tags and end the response</ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
        <CodePane
          textSize='20'
            lang='javascript'
            theme='light'
            source={`// using Express
import { renderToNodeStream } from "react-dom/server"
import MyPage from "./MyPage"

app.get("/", (req, res) => {
  res.write("<!DOCTYPE html><html><head><title>My Page</title></head><body>");
  res.write("<div id='content'>"); 
  const stream = renderToNodeStream(<MyPage/>);
  stream.pipe(res, { end: false });
  stream.on('end', () => {
    res.write("</div></body></html>");
    res.end();
  });
});`} />
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary'>JS Environment</Heading>
          <Layout>
            <Fill>
              <List>
                <ListItem>React 16 uses Map and Set objects (Not array.map)</ListItem>
                <ListItem>Consider using core-js or babel-polyfill</ListItem>
                <ListItem>React also requires requestAnimationFrame - Use raf/polyfill</ListItem>
              </List>
            </Fill>
            <Fill>
              <CodePane
              textSize='20'
              lang='javascript'
              theme='light'
              source={`import 'core-js/es6/map';
import 'core-js/es6/set';

import React from 'react';
import ReactDOM from 'react-dom';
import 'raf/polyfill';

ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);`} />
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary'>Future React</Heading>
          <List>
            <ListItem>React 16 uses Fiber - a complete rewrite of reacts underlying architecture</ListItem>
            <ListItem>This sets up the React team for some awesome improvements in the future, like <a href='https://twitter.com/acdlite/status/909926793536094209?ref_src=twsrc%5Etfw&ref_url=https%3A%2F%2Freactjs.org%2Fblog%2F2017%2F09%2F26%2Freact-v16.0.html'>async react</a></ListItem>
            <ListItem>And also React's Context API - no longer experimental!</ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary'>Building and Deploying Apps</Heading>
          <Text textColor='secondary'>Woot! Cool new features in React and you want to try them out</Text>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary'>Tools</Heading>
          <Text textColor='secondary'>Building apps is easy with the right tools</Text>
          <Text textColor='secondary'>Stop wasting your time with configuring your webpack build.</Text>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary'>Dont use buggy starters</Heading>
          <Text textColor='secondary'>Instead of reaching for a custom starter. like "react-redux-universal-hot-example"</Text>
          <Text textColor='secondary'>Use Toolkits like Create React App</Text>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading textColor='primary'>Options</Heading>
          <List style={{listStyle: 'none'}}>
            <ListItem>Static App => <Link href='https://github.com/facebook/create-react-app'>Create React App</Link></ListItem>
            <ListItem>Isomorphic => <Link href='https://github.com/zeit/next.js/'>Next.js</Link></ListItem>
            <ListItem>Support for muliple framworks => <Link href='https://github.com/jaredpalmer/razzle'>Razzle</Link></ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading size={6} textColor='primary'>Create React App</Heading>
          <Image src='./assets/create-react-app.gif' />
          <Text textColor='secondary'>$ npx create-react-app my-project</Text>
        </Slide>
        <Slide transition={['fade']} bgColor='tertiary'>
          <Heading size={6} textColor='primary'>Deploying To Surge</Heading>
          <Image src='./assets/code-and-deploy.gif' />
          <Text textColor='secondary'>$ npm run build</Text>
          <Text textColor='secondary'>$ npx surge</Text>
        </Slide>
        <Slide transition={['fade']} bgColor='primary' textColor='tertiary'>
          <Heading>Thank You</Heading>        
        </Slide>
        <Slide transition={['fade']} bgColor='primary' textColor='tertiary'>
          <Heading size={6}>Additional Reading</Heading>
          <List>
            <ListItem><Link href='https://reactjs.org/blog/2017/09/26/react-v16.0.html'>React 16</Link></ListItem>
            <ListItem><Link href='https://www.youtube.com/watch?v=v6iR3Zk4oDY'>Whats next with React</Link></ListItem>
            <ListItem><Link href='https://www.youtube.com/watch?v=ZCuYPiUIONs'>React Fiber</Link></ListItem>
            <ListItem><Link href='https://hackernoon.com/whats-new-with-server-side-rendering-in-react-16-9b0d78585d67'>Server Side rendering</Link></ListItem>
            <ListItem><Link href='https://hackernoon.com/using-a-react-16-portal-to-do-something-cool-2a2d627b0202'>React Protals</Link></ListItem>
            <ListItem><Link href='https://hackernoon.com/next-js-razzle-cra-why-you-should-use-them-for-a-next-project-a78d320de97f'>Don't use custom starters</Link></ListItem> 
          </List>
        </Slide>
      </Deck>
    )
  }
}
